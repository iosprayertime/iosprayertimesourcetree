//
//  PrayerTime+CoreDataProperties.swift
//  Prayer Times
//
//  Created by DevoGuru on 22/11/16.
//  Copyright © 2016 Vipul. All rights reserved.
//

import Foundation
import CoreData


extension PrayerTime {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PrayerTime> {
        return NSFetchRequest<PrayerTime>(entityName: "PrayerTime");
    }

 
}
