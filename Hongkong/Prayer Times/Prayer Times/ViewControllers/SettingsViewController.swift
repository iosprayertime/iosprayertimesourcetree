//
//  SettingsViewController.swift
//  Prayer Times
//
//  Created by DevoGuru on 27/10/16.
//  Copyright © 2016 Vipul. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import AVFoundation

class SettingsViewController: UIViewController,AVAudioPlayerDelegate {
    @IBOutlet weak var lblAdustDate: UILabel!
    @IBOutlet weak var btnASAR1: UIButton!
    @IBOutlet weak var btnASAR2: UIButton!
    @IBOutlet weak var btnNotificationSound: UIButton!
    @IBOutlet var pickerNotificationSound: UIPickerView!
    
    @IBOutlet weak var btnPlay: UIButton!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
   
    
    var player: AVAudioPlayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        btnASAR1.isSelected=true
        
        let strNotificationSoundName : String = appDelegate.userDefaults!.value(forKey: "NotificationSoundName") as! String
        self.btnNotificationSound.setTitle(strNotificationSoundName, for: UIControlState.normal)
        
        
        
        let strASAR_CALCULATION_METHOD : String = appDelegate.userDefaults!.value(forKey: "ASAR_CALCULATION_METHOD") as! String
        
        if  strASAR_CALCULATION_METHOD == "ASAR 1"
        {
            btnASAR1.isSelected=true
            btnASAR2.isSelected=false
        }
        else if  strASAR_CALCULATION_METHOD == "ASAR 2"
        {
            btnASAR1.isSelected=false
            btnASAR2.isSelected=true
        }
        
        
        
        let strADJUST_HIJRI_DATE: String = appDelegate.userDefaults!.value(forKey: "ADJUST_HIJRI_DATE") as! String
        
        lblAdustDate.text=strADJUST_HIJRI_DATE
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    
     // MARK: - UIButton Methods
    @IBAction func btnHomeTouch(_ sender: AnyObject) {
        
        dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func btnMinusTouch(_ sender: Any) {
        
        let val:Int? = Int(lblAdustDate.text!)
        
        if val! == -2
        {
            return
        }
        
        let ans = val!-1
        
        
        lblAdustDate.text=String(ans)
        
    }
    
    @IBAction func btnPlusTouch(_ sender: Any) {
        
        let val:Int? = Int(lblAdustDate.text!)
        
        if val! == 2
        {
            return
        }
        let ans = val!+1
        
        
        lblAdustDate.text=String(ans)
    }
    
    
    
    
    @IBAction func btnSaveTouch(_ sender: Any) {
        
        appDelegate.userDefaults!.set(lblAdustDate.text, forKey: "ADJUST_HIJRI_DATE")
        appDelegate.userDefaults!.synchronize()
        
        appDelegate.flagHomeToRefresh=true
        
        self.dismiss(animated: true, completion: nil)
        
//        let deadlineTime = DispatchTime.now() + .seconds(4)
//        DispatchQueue.main.asyncAfter(deadline: deadlineTime, execute: {
//        
//            
//
//        })
        
        
       
        
    }
    
    @IBAction func btnRadioButtonTouch(_ sender: UIButton){
        
        
        appDelegate.flagHomeToRefresh=true
       
        
        if sender == btnASAR1 {
            btnASAR1.isSelected=true
            btnASAR2.isSelected=false
            
            
            appDelegate.userDefaults!.set("ASAR 1" , forKey: "ASAR_CALCULATION_METHOD")
            
        }else
        {
            btnASAR1.isSelected=false
            btnASAR2.isSelected=true
            
             appDelegate.userDefaults!.set("ASAR 2" , forKey: "ASAR_CALCULATION_METHOD")
        }
    }
   
    @IBAction func btnNotificationSoundTouch(_ sender: Any) {
        
        
        if (btnPlay.isSelected==true)
        {
            
            btnPlay.isSelected=false
            
            guard let player = player else { return }
            player.stop()
            
            
        }

       
         let strNotificationSoundName : String = appDelegate.userDefaults!.value(forKey: "NotificationSoundName") as! String
        
        let intIndex:Int=Constants.arrNotificationSoundName.index{$0 == strNotificationSoundName}!
        
        
        
        
        ActionSheetStringPicker.show(withTitle: "Select Sound", rows: Constants.arrNotificationSoundName , initialSelection: intIndex, doneBlock: {
            picker, value, index in
            
//            print("value = \(value)")
//            print("index = \(index)")
//            print("picker = \(picker)")
            
             print("Name = ",Constants.arrNotificationSoundName[value] ?? "" )
             print("File = ",Constants.arrNotificationSoundFile[value] ?? "")
            
            self.appDelegate.flagHomeToRefresh=true
            
       
            
            
            self.appDelegate.userDefaults!.set(Constants.arrNotificationSoundName[value] , forKey: "NotificationSoundName")
            
            self.btnNotificationSound.setTitle(Constants.arrNotificationSoundName[value], for: UIControlState.normal)
            
            
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)

        
    }
    
    @IBAction func btnPlayTouch(_ sender: Any) {
        
        
        if (btnPlay.isSelected==true)
        {
            
            btnPlay.isSelected=false
            
            guard let player = player else { return }
            player.stop()
            
        
        }
        else
        {
            
            btnPlay.isSelected=true
            
            let strNotificationSoundName : String = appDelegate.userDefaults!.value(forKey: "NotificationSoundName") as! String
            
            
            
            print("fileName to Play = \(strNotificationSoundName)")
            
            
            
            let url = Bundle.main.url(forResource: strNotificationSoundName, withExtension: "wav")!
            
            do {
                player = try AVAudioPlayer(contentsOf: url)
                guard let player = player else { return }
                player.delegate=self
                player.prepareToPlay()
                player.play()
                
            } catch let error {
                print(error.localizedDescription)
            }

        }
        
        
    }
    
     // MARK: - AVAudioPlayerDelegate
    public func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool)
    {
        
        if (flag) {
             btnPlay.isSelected=false
        }
        
    }
    public func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        
    }
}
