//
//  AppDelegate.swift
//  Prayer Times
//
//  Created by DevoGuru on 24/10/16.
//  Copyright © 2016 Vipul. All rights reserved.
//

import UIKit

import Firebase
import GoogleMobileAds
import  UserNotifications

import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
   
    let userDefaults = UserDefaults(suiteName: "group.com.quthubuzaman.kayalprayertimes")
//    var monthDict = JSON.null
    var intdayString: Int=0
    var numDays: Int=0
    
    
    var monthString:String=""
    var dayString:String=""
    var yearString:String=""
    
    
    var flagHomeToRefresh:Bool=false
    
     
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
       
        
        // Use Firebase library to configure APIs
        FIRApp.configure()
        // Initialize Google Mobile Ads SDK
        GADMobileAds.configure(withApplicationID: "ca-app-pub-4152079293119594~9021169865")
        
        
        
        
        flagHomeToRefresh = true
        
        
        
        //Requesting Authorization for User Interactions
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options: [.alert,.badge, .sound]) { (granted, error) in
                // Enable or disable features based on authorization.
            }
            

        } else {
            // Fallback on earlier versions
            

            let mySettings = UIUserNotificationSettings(types:  [.alert,.badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(mySettings)
        }
        
       // GADMobileAds.configure(withApplicationID: Constants.kAdUnit);
 
  

        // NotificationSoundName
        
        if  ((userDefaults!.value(forKey: "NotificationSoundName")) == nil)
        {
            userDefaults!.set("thakbeer", forKey: "NotificationSoundName")
        }
        else
        {
            let result = userDefaults!.value(forKey: "NotificationSoundName")
           print("UserDefaults NotificationSoundName : " ,result!)
        }
        
        // ASAR_CALCULATION_METHOD
        
        if  ((userDefaults!.value(forKey: "ASAR_CALCULATION_METHOD")) == nil)
        {
            userDefaults!.set("ASAR 1", forKey: "ASAR_CALCULATION_METHOD")
        }
        else
        {
            let result = userDefaults!.value(forKey: "ASAR_CALCULATION_METHOD")
            print("UserDefaults ASAR_CALCULATION_METHOD : " ,result!)
        }
        
        
       // ADJUST HIJRI DATE
        
        
        if  ((userDefaults!.value(forKey: "ADJUST_HIJRI_DATE")) == nil)
        {
            userDefaults!.set("0", forKey: "ADJUST_HIJRI_DATE")
        }
        else
        {
            let result = userDefaults!.value(forKey: "ADJUST_HIJRI_DATE")
            print("UserDefaults ADJUST_HIJRI_DATE : " ,result!)
        }
        
        
        // Subuh  Button
        
        
        if  ((userDefaults!.value(forKey: "Subuh")) == nil)
        {
            userDefaults!.set("YES", forKey: "Subuh")
        }
        else
        {
            let result = userDefaults!.value(forKey: "Subuh")
            print("UserDefaults Subuh : " ,result!)
        }

        
        // Zuhr Button
        
        
        if  ((userDefaults!.value(forKey: "Zuhr")) == nil)
        {
            userDefaults!.set("YES", forKey: "Zuhr")
        }
        else
        {
            let result = userDefaults!.value(forKey: "Zuhr")
            print("UserDefaults Zuhr : " ,result!)
        }
        
        
        
        // Asar Button
        
        
        if  ((userDefaults!.value(forKey: "Asar")) == nil)
        {
            userDefaults!.set("YES", forKey: "Asar")
        }
        else
        {
            let result = userDefaults!.value(forKey: "Asar")
            print("UserDefaults Asar : " ,result!)
        }
        
        
        
        // Maghrib Button
        
        
        if  ((userDefaults!.value(forKey: "Maghrib")) == nil)
        {
            userDefaults!.set("YES", forKey: "Maghrib")
        }
        else
        {
            let result = userDefaults!.value(forKey: "Maghrib")
            print("UserDefaults Subuh : " ,result!)
        }
        
        
        // Isha Button
        
        
        if  ((userDefaults!.value(forKey: "Isha")) == nil)
        {
            userDefaults!.set("YES", forKey: "Isha")
        }
        else
        {
            let result = userDefaults!.value(forKey: "Isha")
            print("UserDefaults Isha : " ,result!)
        }
        
//        // Sunrise Button
//        if  ((userDefaults!.value(forKey: "Sunrise")) == nil)
//        {
//            userDefaults!.set("YES", forKey: "Sunrise")
//        }
//        else
//        {
//            let result = userDefaults!.value(forKey: "Sunrise")
//            print("UserDefaults Isha : " ,result!)
//        }
        
        
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "MMMM"
        
        let currentDate=Date()
        
        monthString = dayTimePeriodFormatter.string(from: currentDate)
        
        monthString=monthString.lowercased()
        
        print("Date :",monthString);
        
      
        
        dayTimePeriodFormatter.dateFormat = "d"
         dayString = dayTimePeriodFormatter.string(from: currentDate)
        print("Current Day :",dayString);
        
        
        dayTimePeriodFormatter.dateFormat = "YYYY"
         yearString = dayTimePeriodFormatter.string(from: currentDate)
        print("Current Year :",yearString);

        
        
     
        let calendar = Calendar.current
        
        let range = calendar.range(of: .day, in: .month, for: currentDate)!
       
        numDays = range.count
//        print("numDays :",numDays) // 31
        
        
       intdayString = Int(dayString)!
        
        //dateString now contains the string "Sunday, 7 AM".
        
        
        let totalRecords:Int = CoreDataStack.recordsInTable(PrayerTime.self)
        
        if (totalRecords > 0)
        {
            print("Records Found ", totalRecords )
        }
        else
        {
            //myCoreDataObj.deleteAllRecords(PrayerTime.self)
            
            
            print("Records Not Found ")
                // kayaldata  . txt    chennaidata   . txt
            if let path = Bundle.main.path(forResource: "chennaidata", ofType: "txt") {
                do {
                    let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                    
                    
                    let parsedData = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:Any]
                    
                    //print(parsedData["january"] as Any);
                    
                    
                    
                    for monthname in Constants.arrMonthName {
                        
                        print(monthname! as String);
                        
                        let dic:NSDictionary = parsedData[monthname!] as! NSDictionary
                        
                        // print(dic)
                        
                        for i in (1..<31+1)
                        {
                            let strDate=String(i)
                            if ((dic[strDate]) != nil)
                            {
                                let dicValues:NSDictionary = dic[strDate] as! NSDictionary
                                
                                //print("Date ", strDate, "Value ",dicValues)
                                
                                
                                let valAsr_Hanafi:String = dicValues[Constants.KEYS_Asr_Hanafi] as! String ;
                                let valAsr_Shafi:String = dicValues[Constants.KEYS_Asr_Shafi] as! String ;
                                let valIsha_Hanafi:String = dicValues[Constants.KEYS_Isha_Hanafi] as! String ;
                                let valIsha_Shafi:String = dicValues[Constants.KEYS_Isha_Shafi] as! String ;
                                let valZuhr :String = dicValues[Constants.KEYS_Zuhr] as! String ;
                                let valMaghrib:String = dicValues[Constants.KEYS_Maghrib] as! String ;
                                let valsubuh:String = dicValues[Constants.KEYS_Subuh] as! String ;
                                let valsunrise :String = dicValues[Constants.KEYS_Sunrise] as! String ;
                                
                                
                                
                                let p = CoreDataStack.addRecord(PrayerTime.self)
                                
                                p.address=""
                                p.area=""
                                
                                p.dayDigit=Int16(i)
                                p.day=strDate
                                p.month=monthname
                                p.year=yearString
                                
                                p.asar_hanafi=valAsr_Hanafi
                                p.asar_shafi=valAsr_Shafi
                                p.isha_hanafi=valIsha_Hanafi
                                p.isha_shafi=valIsha_Shafi
                                
                                p.luhar=valZuhr
                                p.magrib=valMaghrib
                                p.subuh=valsubuh
                                p.sunrise=valsunrise
                                
                                
                                CoreDataStack.saveDatabase()
                                
                                
                                
                            }
                            
                        }
                        
                        
                        
                    }
                    
                    print("Final");
                    
                    
                } catch let error {
                    print(error.localizedDescription)
                }
            } else {
                print("Invalid filename/path.")
            }
            
            
             print("Records Inserted from File .. Done.. ")
        }
        
        
      

        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

//    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void){
//        
//        
//        print ("willPresent notification")
//        
//        
//
//        
//    }
    
    
    // MARK: - Core Data stack
    
    lazy var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.appcoda.CoreDataDemo" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "PrayerCoreData", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("PrayerCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
    
    func deleteAll(fromEntity entityName: String) {
        
        if ((UIApplication.shared.delegate as? AppDelegate)?.managedObjectContext) != nil {
            
            let fetchRequest =  NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
            //3
            
            
            if #available(iOS 9.0, *) {
                let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
                
                do {
                    try managedObjectContext.execute(deleteRequest)
                    try managedObjectContext.save()
                    //5
                } catch {
                    print (error)
                }
            }
            else {
                // Fallback on earlier versions
                do {
                    
                    let commentIDs =  try managedObjectContext.fetch(fetchRequest) as! [NSManagedObject]
                    //8
                    for comment in commentIDs {
                        managedObjectContext.delete(comment)
                        
                    }
                    //9
                    //4
                    
                    try managedObjectContext.save()
                    //5
                } catch {
                    print (error)
                }
            }
            
        }
        
        
    }

    // MARK: - UserDefaults isAhar1
    
    
    func isAhar1() -> Bool {
        
        let strASAR : String = userDefaults!.value(forKey: "ASAR_CALCULATION_METHOD") as! String
        
        
        if  strASAR == "ASAR 1"
        {
            return true
        }
        else
        {
            return false
        }

    }
    
}
