//
//  CoreDataStack.swift
//  Prayer Times
//
//  Created by DevoGuru on 18/01/17.
//  Copyright © 2017 Vipul. All rights reserved.
//

import UIKit
import CoreData
class CoreDataStack: NSObject {

    
    // MARK: - Core Data stack
    
    
    static var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.appcoda.CoreDataDemo" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    
    static var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "PrayerCoreData", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    static var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        let url = applicationDocumentsDirectory.appendingPathComponent("PrayerCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()
    
    static var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()


    
    
 
    
    
    static func addRecord<T: NSManagedObject>(_ type : T.Type) -> T
    {
        let entityName = T.description()
        let context = CoreDataStack.managedObjectContext
        let entity = NSEntityDescription.entity(forEntityName: entityName, in: context)
        let record = T(entity: entity!, insertInto: context)
        return record
    }
    
    static func recordsInTable<T: NSManagedObject>(_ type : T.Type) -> Int
    {
        
        let recs = CoreDataStack.allRecords(T.self)
        return recs.count
    }
    
    
    static func allRecords<T: NSManagedObject>(_ type : T.Type, sort: NSSortDescriptor? = nil) -> [T]
    {
        let context = CoreDataStack.managedObjectContext
        let request: NSFetchRequest<T>
        if #available(iOS 10.0, OSX 10.12, *) {
            request = T.fetchRequest() as! NSFetchRequest<T>
        } else {
            request = NSFetchRequest(entityName: "T")
        }
        do
        {
            let results = try context.fetch(request)
            return results as! [T]
        }
        catch
        {
            print("Error with request: \(error)")
            return []
        }
    }
    
    static func query<T: NSManagedObject>(_ type : T.Type, search: NSPredicate?, sort: NSSortDescriptor? = nil, multiSort: [NSSortDescriptor]? = nil) -> [T]
    {
        let context = CoreDataStack.managedObjectContext
        let request: NSFetchRequest<T>
        if #available(iOS 10.0, OSX 10.12, *) {
            request = T.fetchRequest() as! NSFetchRequest<T>
        } else {
            request = NSFetchRequest(entityName: "T")
        }
        
        if let predicate = search
        {
            request.predicate = predicate
        }
        if let sortDescriptors = multiSort
        {
            request.sortDescriptors = sortDescriptors
        }
        else if let sortDescriptor = sort
        {
            request.sortDescriptors = [sortDescriptor]
        }
        
        do
        {
            let results = try context.fetch(request)
            return results as! [T]
        }
        catch
        {
            print("Error with request: \(error)")
            return []
        }
    }
    
    
    static func deleteRecord(_ object: NSManagedObject)
    {
        let context = CoreDataStack.managedObjectContext
        context.delete(object)
    }
    
    static func deleteAllRecords<T: NSManagedObject>(_ type : T.Type)
    {
        
        deleteRecords(T.self)
    }
    
    static func deleteRecords<T: NSManagedObject>(_ type : T.Type, search: NSPredicate? = nil)
    {
        let context = CoreDataStack.managedObjectContext
        
        let results = query(T.self, search: search)
        for record in results
        {
            context.delete(record)
        }
    }
    
    static func saveDatabase()
    {
        let context = CoreDataStack.managedObjectContext
        
        do
        {
            try context.save()
        }
        catch
        {
            print("Error saving database: \(error)")
        }
    }


}
