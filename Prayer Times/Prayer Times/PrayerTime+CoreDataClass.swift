//
//  PrayerTime+CoreDataClass.swift
//  Prayer Times
//
//  Created by DevoGuru on 22/11/16.
//  Copyright © 2016 Vipul. All rights reserved.
//

import Foundation
import CoreData

@objc(PrayerTime)
public class PrayerTime: NSManagedObject {
    
    @NSManaged public var address: String?
    @NSManaged public var area: String?
    @NSManaged public var asar_hanafi: String?
    @NSManaged public var asar_shafi: String?
    @NSManaged public var day: String?
    @NSManaged public var isha_hanafi: String?
    @NSManaged public var isha_shafi: String?
    @NSManaged public var luhar: String?
    @NSManaged public var magrib: String?
    @NSManaged public var month: String?
    @NSManaged public var subuh: String?
    @NSManaged public var sunrise: String?
    @NSManaged public var year: String?
    @NSManaged public var dayDigit: Int16


}
