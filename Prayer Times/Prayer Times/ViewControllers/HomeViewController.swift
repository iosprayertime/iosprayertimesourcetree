//
//  ViewController.swift
//  Prayer Times
//
//  Created by DevoGuru on 24/10/16.
//  Copyright © 2016 Vipul. All rights reserved.
//

import UIKit
import WatchKit
import WatchConnectivity

import GoogleMobileAds
import  UserNotifications
import UserNotificationsUI

import CoreData
import AVFoundation

extension Date {
    func startOfMonth() -> Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
    }
    
    func endOfMonth() -> Date {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth())!
    }
}

class HomeViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate,GADBannerViewDelegate, WCSessionDelegate {
   
    
    var player: AVAudioPlayer?
    

    var interstitial: GADInterstitial!
    
//    @IBOutlet weak var lblDate: UILabel!
//    
//    @IBOutlet weak var lblDate_islamic: UILabel!
    
    
    @IBOutlet weak var btnStop: UIButton!
    
     @IBOutlet weak var lblRemaingTimeDigit: UILabel!
    @IBOutlet weak var lblRemaingTime: UILabel!
    @IBOutlet weak var collectionview: UICollectionView!
    
    @IBOutlet var viewPager: ViewPager!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var dicDays:NSMutableDictionary = NSMutableDictionary()
    var selectedIndex:Int = 0
    
    let reuseIdentifier = "cellCalDay"
   
    
    var arrViewPagesAll:NSMutableArray = []
    
    var objPrayerTimeCurrent:PrayerTime? = nil
    var objPrayerTimeCurrentNextDay:PrayerTime? = nil
    
    func nextarrvalue(strmonth:String , intday : Int) -> [PrayerTime?]  {
       
        let resultPredicate1_nextday: NSPredicate = NSPredicate(format: "month = %@ and dayDigit = %d", strmonth, intday)
        
        let arrResult_nextday:[PrayerTime?]  = CoreDataStack.query(PrayerTime.self, search: resultPredicate1_nextday, sort: nil, multiSort: nil )
        
        return arrResult_nextday
    }
  

    func fetch() {
        
        
        
         UIApplication.shared.cancelAllLocalNotifications()
        
        
        
        let resultPredicate1: NSPredicate = NSPredicate(format: "month = %@ and dayDigit >= %d", appDelegate.monthString, appDelegate.intdayString)
        
        let resultSort: NSSortDescriptor=NSSortDescriptor(key: "dayDigit", ascending: true)
        
        let arrResult:[PrayerTime?]  = CoreDataStack.query(PrayerTime.self, search: resultPredicate1, sort: resultSort, multiSort: nil )
        
  
        
        
        var arrResult_nextday:[PrayerTime?]  = nextarrvalue(strmonth: appDelegate.monthString , intday:appDelegate.intdayString + 1 )
        
        
        if ( arrResult_nextday.count > 0 )
        {
        
        }
        else{
            
            let indx:Int = Constants.arrMonthName.index(where: { $0 ==  appDelegate.monthString })!
            
            arrResult_nextday = nextarrvalue(strmonth: Constants.arrMonthName[indx+1]! , intday: 1 )
            
        }
        
        
        objPrayerTimeCurrentNextDay=arrResult_nextday[0]
        
        
        arrViewPagesAll.removeAllObjects()
        
        var indexValue: Int = 0
        for val in arrResult {
            
            //print("Day",val?.day as Any)
            let objPrayerTime:PrayerTime = val!
            
            
            
            
            let viewpage = UINib(nibName: "ViewPagerCustom", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! ViewPagerCustom
            
            let Subuh : String = objPrayerTime.subuh!
            let Zuhr: String = objPrayerTime.luhar!
            let Asar_Hanafi: String = objPrayerTime.asar_hanafi!
            let Asar_Shafi: String = objPrayerTime.asar_shafi!
            let Maghrib: String = objPrayerTime.magrib!
            let Isha_Hanafi: String = objPrayerTime.isha_hanafi!
            let Isha_Shafi: String = objPrayerTime.isha_shafi!
            let Sunrise: String = objPrayerTime.sunrise!
            
            
            
            let today=Date()
            let DateValue = Calendar.current.date(byAdding: .day, value: indexValue, to: today)
            
            
            viewpage.lblDate.text = Show_currentDate(DateValue!)
            viewpage.lblDate_islamic.text = Show_IslamicDate(DateValue!)
            
            
            indexValue = indexValue + 1
            
            
            
            viewpage.lblTimeSubuh.text = Subuh + " AM"
            viewpage.lblTimeZuhr.text = Zuhr+" PM"
            
            if(appDelegate.isAhar1())
            {
                viewpage.lblTimeAsar.text = Asar_Hanafi + " PM"
                viewpage.lblTimeIsha.text = Isha_Hanafi+" PM"
            }
            else
            {
                viewpage.lblTimeAsar.text =  Asar_Shafi + " PM"
                viewpage.lblTimeIsha.text = Isha_Shafi+" PM"
            }
            
            viewpage.lblTimeMaghrib.text = Maghrib+" PM"
            
            viewpage.lblTimeSunrise.text = Sunrise+" AM"
            
            viewpage.btnAsar.addTarget(self, action: #selector(pressButton(button:)), for: .touchUpInside)
            viewpage.btnIsha.addTarget(self, action: #selector(pressButton(button:)), for: .touchUpInside)
            viewpage.btnZuhr.addTarget(self, action: #selector(pressButton(button:)), for: .touchUpInside)
            viewpage.btnSubuh.addTarget(self, action: #selector(pressButton(button:)), for: .touchUpInside)
            viewpage.btnMaghrib.addTarget(self, action: #selector(pressButton(button:)), for: .touchUpInside)
            
            
            viewpage.isUserInteractionEnabled=true;
            
            arrViewPagesAll.add(viewpage)
            
            if Int(objPrayerTime.dayDigit) == appDelegate.intdayString {
                
                appDelegate.userDefaults!.set(viewpage.lblTimeSubuh.text, forKey: "TimeSubuh")
                appDelegate.userDefaults!.set(viewpage.lblTimeZuhr.text, forKey: "TimeZuhr")
                appDelegate.userDefaults!.set(viewpage.lblTimeAsar.text, forKey: "TimeAsar")
                appDelegate.userDefaults!.set(viewpage.lblTimeMaghrib.text, forKey: "TimeMaghrib")
                appDelegate.userDefaults!.set(viewpage.lblTimeIsha.text, forKey: "TimeIsha")
                appDelegate.userDefaults!.set(viewpage.lblTimeSunrise.text, forKey: "TimeSunrise")
                
                
                
                
                objPrayerTimeCurrent=objPrayerTime
                
               // objPrayerTimeCurrentNextDay=arrResult[i+1]
                
                
                Show_RemainingTime()
                
                
               
                
                
               Timer.scheduledTimer(timeInterval:1, target: self, selector: #selector(Show_RemainingTime), userInfo: nil, repeats: true)
                
                
                
                
                
            }
            
            
            // Add Object for Notification
            
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM-dd-yyyy"
            
            let dateString = dateFormatter.string(from:DateValue! as Date)
            
            print("addNotification " + dateString)
            
            
            addNotification(objPrayerTimeForNotify: objPrayerTime ,strDate: dateString )
            
           
            
            
            
        }
        
//        /// Start ---   Test Notification   ////
//        
//        
//       
//        
//        testTime(strTimeToDo: "12:35 p")
//        testTime(strTimeToDo: "12:36 p")
//        testTime(strTimeToDo: "12:37 p")
//        testTime(strTimeToDo: "12:38 p")
//        testTime(strTimeToDo: "12:39 p")
//        testTime(strTimeToDo: "12:40 p")
//        testTime(strTimeToDo: "12:41 p")
//        testTime(strTimeToDo: "12:42 p")
//        testTime(strTimeToDo: "12:43 p")
//        testTime(strTimeToDo: "12:44 p")
//        testTime(strTimeToDo: "12:45 p")
//        
//        /// End ---   Test Notification   ////

        
        viewPager.reloadData()
        
        viewPager.isUserInteractionEnabled=true;
        
    }

    
    private func createAndLoadInterstitial() {
        interstitial = GADInterstitial(adUnitID: Constants.kAdUnit)
        let request = GADRequest()
        // Request test ads on devices you specify. Your test device ID is printed to the console when
        // an ad request is made.
        #if TARGET_IPHONE_SIMULATOR
            request.testDevices = [ kGADSimulatorID, "2077ef9a63d2b398840261c8221a0c9b" ]
        #else
           
        #endif

        
        interstitial.load(request)
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        
        let nibName = UINib(nibName: "cellCalDay", bundle:nil)
        collectionview.register(nibName, forCellWithReuseIdentifier: "cellCalDay")
        
//        if  appDelegate.flagHomeToRefresh == true
//        {
//            appDelegate.flagHomeToRefresh = false
//            
//        }
//        else
//        {
//            
//
//        }
      self.lblRemaingTimeDigit.text = ""
        self.lblRemaingTime.text = ""
        fetch()
     
        
        //        let currentDate=Date()
        //        Show_currentDate(currentDate)
        //        Show_IslamicDate(currentDate)
        
       
//        print(dicDays)

        createAndLoadInterstitial()
        
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        viewPager.dataSource = self;
        
//
//        
//        print("Google Mobile Ads SDK version: \(GADRequest.sdkVersion())")


        
        
        
        var timer = Timer.scheduledTimer(timeInterval: 300.0, target: self, selector: #selector(self.loadAdvertise), userInfo: nil, repeats: true)
        
        
           }
    
    
    public func testTime ( strTimeToDo :String)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy"
        
        let dateString = dateFormatter.string(from:NSDate() as Date)
    
        let dateObj1:Date = generateDateFromStringForNotification(strDate:dateString, strTime: strTimeToDo )
        
        let elapsed1 = dateObj1.timeIntervalSince(NSDate() as Date)
        if  (elapsed1 > 0 )
        {
            
            triggerNotificationTest(timevalue: elapsed1 ,strTitle: strTimeToDo);
        }
    
    }
    
   
    
//    // MARK: - touches Events
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        guard let player = player else { return }
//        player.stop()
//    }
    
    
    
    @IBAction func btnStop_Click(_ sender: Any) {
    }
    
    // MARK: - loadAdvertise
    public func loadAdvertise()
    {
        if interstitial.isReady {
            interstitial.present(fromRootViewController: self)
        } else {
           // print("Ad wasn't ready")
            
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let player = player else { return }
        player.stop()

        
//        if "HomeToInfo" == segue.identifier {
//            
//
//            
//        }
//        else if "HomeToInfo" == segue.identifier {
//            
//
//            
//        }
        
    }
   
    
    // MARK: - WCSession

    /** Called when the session can no longer be used to modify or add any new transfers and, all interactive messages will be cancelled, but delegate callbacks for background transfers can still occur. This will happen when the selected watch is being changed. */
    @available(iOS 9.3, *)
    public func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    /** Called when all delegate callbacks for the previously selected watch has occurred. The session can be re-activated for the now selected watch using activateSession. */
    @available(iOS 9.3, *)
    public func sessionDidDeactivate(_ session: WCSession) {
        
    }
    
    
    
    /** Called when the session has completed activation. If session state is WCSessionActivationStateNotActivated there will be an error with more details. */
    @available(iOS 9.3, *)
    public func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }

   
    // MARK: - Show Functions
    
    
    func Show_DateDayFirstCharacter(_ date: Date) -> String {
        
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "EE"
        
        let currentDate=date
        
        let dateString = dayTimePeriodFormatter.string(from: currentDate)
        
        return dateString
        
    }
     func Show_selectedView(_ indexValue: Int)
     {
        
//        let today=Date()
//        let DateValue = Calendar.current.date(byAdding: .day, value: indexValue, to: today)
        
//        Show_currentDate(DateValue!)
//        Show_IslamicDate(DateValue!)
        
        selectedIndex=indexValue
        
        collectionview.reloadData()
        DispatchQueue.main.async( execute: {
           // print("centeredHorizontally")
            
            let indexPath = NSIndexPath(row: indexValue, section: 0)
            self.collectionview.scrollToItem(at: indexPath as IndexPath, at:  .centeredHorizontally, animated: true)
            
        })
    
    }
    func Show_currentDate(_ date: Date)-> String {
        
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "EEE,dd MMM YYYY"
        
        let currentDate=date
        
        let dateString = dayTimePeriodFormatter.string(from: currentDate)
        
//        lblDate.text = dateString
        return dateString
        
    }
    func Show_IslamicDate(_ date: Date)-> String {
        
        
        let strHIJRI_DATE : String = appDelegate.userDefaults!.value(forKey: "ADJUST_HIJRI_DATE") as! String
        
        let dayToAdd:Double = Double(strHIJRI_DATE)!
        
//        dateByAddingTimeInterval:60*60*24*daysToAdd
        let newDate: Date = date.addingTimeInterval(60 * 60 * 24 * dayToAdd)
        
        let myLocale = Locale .current
        var calendar = Calendar(identifier: .islamic)
        calendar.locale = myLocale
        
        
        //: ### Fetching `DateComponents` off a `Date`
        //: Notice how *a locale is needed for the month symbols to be reported correctly*
        let dateComponents = calendar.dateComponents([.day, .month, .year], from: newDate)
        
//        let monthName = calendar.shortMonthSymbols[dateComponents.month! - 1]
        
        let monthName:String = Constants.arrMonthNameIslamic[dateComponents.month! - 1]!
        
        let islamicName="\(dateComponents.day!)-\(monthName)-\(dateComponents.year!)"
//        lblDate_islamic.text = islamicName
        
        
        return islamicName;
        
    }
    
    func Show_RemainingTime()
    {
        
        var Subuh : String = objPrayerTimeCurrent!.subuh!
        var Zuhr: String = objPrayerTimeCurrent!.luhar!
        let Asar_Hanafi: String = objPrayerTimeCurrent!.asar_hanafi!
        let Asar_Shafi: String = objPrayerTimeCurrent!.asar_shafi!
        var Maghrib: String = objPrayerTimeCurrent!.magrib!
        let Isha_Hanafi: String = objPrayerTimeCurrent!.isha_hanafi!
        let Isha_Shafi: String = objPrayerTimeCurrent!.isha_shafi!
        var Sunrise: String = objPrayerTimeCurrent!.sunrise!
        
       
        
        Subuh=Subuh+" a"
        Zuhr=Zuhr+" p"
        var Asar: String = ""
        var Isha: String = ""
        if(appDelegate.isAhar1())
        {
            Asar = Asar_Hanafi + " p"
            Isha = Isha_Hanafi + " p"
        }
        else
        {
            Asar =  Asar_Shafi + " p"
            Isha = Isha_Shafi + " p"
        }
        Maghrib=Maghrib+" p"
        
        Sunrise=Sunrise+" a"
        
        
        
        var arrDate:[Date]=[]
        var arrValues:[String]=[]
       
        var dateObj:Date = generateDateFromString(strDate: Subuh)
        
        arrDate.append(dateObj)
        arrValues.append(Subuh)
        
        dateObj = generateDateFromString(strDate:  Zuhr)
        
        arrDate.append(dateObj)
        arrValues.append(Zuhr)
        
        dateObj = generateDateFromString(strDate: Asar)
        
        arrDate.append(dateObj)
        arrValues.append(Asar)
        
        dateObj = generateDateFromString(strDate: Maghrib)
        
        arrDate.append(dateObj)
        arrValues.append(Maghrib)
        
        dateObj = generateDateFromString(strDate:  Isha)
        
        arrDate.append(dateObj)
        arrValues.append(Isha)
        
        dateObj = generateDateFromString(strDate:  Sunrise)
        
        arrDate.append(dateObj)
        arrValues.append(Sunrise)
        //  print("Dateobj: \(dateObj)")
        
        var flag:Bool = false
        
        
        let viewpage:ViewPagerCustom = arrViewPagesAll.object(at: 0) as! ViewPagerCustom
        
        viewpage.lblSubuh.textColor=Constants.Color_Default
        viewpage.lblZuhr.textColor=Constants.Color_Default
        viewpage.lblAsar.textColor=Constants.Color_Default
        viewpage.lblMaghrib.textColor=Constants.Color_Default
        viewpage.lblIsha.textColor=Constants.Color_Default
        viewpage.lblSunrise.textColor=Constants.Color_Default
        
        
        viewpage.lblTimeSubuh.textColor=Constants.Color_Default
        viewpage.lblTimeZuhr.textColor=Constants.Color_Default
        viewpage.lblTimeAsar.textColor=Constants.Color_Default
        viewpage.lblTimeMaghrib.textColor=Constants.Color_Default
        viewpage.lblTimeIsha.textColor=Constants.Color_Default
        viewpage.lblTimeSunrise.textColor=Constants.Color_Default
        
        for  i  in (0..<5) {
            
            let d:Date=arrDate[i]
            
            let curr:Date = NSDate() as Date
            
            if d > curr
            {
                
                flag = true
                
//                print("Date greater")
                
                let strDateDiff:String = dateDiff(startDate:d , endDate:NSDate() as Date )
//                print("strDateDiff:: \(strDateDiff)")
                self.lblRemaingTimeDigit.text = strDateDiff
                self.lblRemaingTime.text = " to " + Constants.arrKeysToDisplay[i]
                
               
                
                
                switch i {
                case 0:
                    viewpage.lblSubuh.textColor=Constants.Color_Highlighted
                    viewpage.lblTimeSubuh.textColor=Constants.Color_Highlighted
                    break
                case 1:
                    viewpage.lblZuhr.textColor=Constants.Color_Highlighted
                    viewpage.lblTimeZuhr.textColor=Constants.Color_Highlighted
                    break
                case 2:
                    viewpage.lblAsar.textColor=Constants.Color_Highlighted
                    viewpage.lblTimeAsar.textColor=Constants.Color_Highlighted
                    break
                case 3:
                    viewpage.lblMaghrib.textColor=Constants.Color_Highlighted
                    viewpage.lblTimeMaghrib.textColor=Constants.Color_Highlighted
                    break

                case 4:
                    viewpage.lblIsha.textColor=Constants.Color_Highlighted
                    viewpage.lblTimeIsha.textColor=Constants.Color_Highlighted
                    break
                case 5:
                    viewpage.lblSunrise.textColor=Constants.Color_Highlighted
                    viewpage.lblTimeSunrise.textColor=Constants.Color_Highlighted
                    break

                default:
                    break
                }
                
                
                
                appDelegate.userDefaults!.set(Constants.arrKeysToDisplay[i], forKey: "TimeActive")
                
            
                appDelegate.userDefaults!.set(self.lblRemaingTime.text , forKey: "RemaingTime")
                
                
                
                let strWatch_TimeActive_Title:String = Constants.arrKeysToDisplay[i]
                let strWatch_TimeActive_Time:String = dateDiffWatch(startDate:d , endDate:NSDate() as Date )
                var strWatch_Previous_Title:String = ""
                var strWatch_Previous_Time:String = ""
                
                
                if ( i > 0 )
                {
                    strWatch_Previous_Title = Constants.arrKeysToDisplay[i-1]
                    strWatch_Previous_Time = arrValues[i-1]
                }
                
                var strWatch_Next_Title:String = ""
                var strWatch_Next_Time:String = ""
                
                if ( i < 5)
                {
                    strWatch_Next_Title = Constants.arrKeysToDisplay[i+1]
                    strWatch_Next_Time = arrValues[i+1]
                }
               
                
                
                
                if #available(iOS 9.0, *) {
                    if WCSession.isSupported() { //makes sure it's not an iPad or iPod
                        let watchSession = WCSession.default()
                        watchSession.delegate = self
                        watchSession.activate()
                        if watchSession.isPaired && watchSession.isWatchAppInstalled {
                            do {
                                try watchSession.updateApplicationContext(["Watch_TimeActive_Title":strWatch_TimeActive_Title,
                                                                           "Watch_TimeActive_Time":strWatch_TimeActive_Time,
                                                                           "Watch_Previous_Title":strWatch_Previous_Title,
                                                                           "Watch_Previous_Time":strWatch_Previous_Time,
                                                                           "Watch_Next_Title":strWatch_Next_Title,
                                                                           "Watch_Next_Time":strWatch_Next_Time])
                            } catch let error as NSError {
                                print(error.description)
                            }
                        }
                    }
                } else {
                    // Fallback on earlier versions
                }
                
                
                break
                
                //            let newdateString = dateFormatter.string(from:dateObj)
                //            print(newdateString)
                
            }
            else
            {
//                print("Date less ")                
            }

        }
        
        if ( flag == false)
        {
             var Subuh : String = objPrayerTimeCurrentNextDay!.subuh!
             Subuh=Subuh+" a"
            
            
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM-dd-yyyy"
            
         
            let nextday : Date = Calendar.current.date(byAdding: DateComponents(day: 1), to: NSDate() as Date)!
            
            let dateString = dateFormatter.string(from:nextday)
            //        print(dateString)
            
            let strDateToConvert:String = dateString + " " + Subuh
            
            
            
            // print("strDate: \(strDateSubuh)")
            dateFormatter.dateFormat = "MM-dd-yyyy hh:mm a"
            
            let dateObj:Date  = dateFormatter.date(from: strDateToConvert)! as Date
            
            
            let strDateDiff:String = dateDiff(startDate:dateObj , endDate:NSDate() as Date )
            //                print("strDateDiff:: \(strDateDiff)")
            
            
           
            self.lblRemaingTimeDigit.text = strDateDiff
            self.lblRemaingTime.text = " to " + Constants.arrKeysToDisplay[0]
            
            
            
            
            
            if (arrViewPagesAll.count > 1 )
            {
                let viewpage:ViewPagerCustom = arrViewPagesAll.object(at: 1) as! ViewPagerCustom
                
                viewpage.lblSubuh.textColor=Constants.Color_Default
                viewpage.lblZuhr.textColor=Constants.Color_Default
                viewpage.lblAsar.textColor=Constants.Color_Default
                viewpage.lblMaghrib.textColor=Constants.Color_Default
                viewpage.lblIsha.textColor=Constants.Color_Default
                viewpage.lblSunrise.textColor=Constants.Color_Default
                
                
                viewpage.lblTimeSubuh.textColor=Constants.Color_Default
                viewpage.lblTimeZuhr.textColor=Constants.Color_Default
                viewpage.lblTimeAsar.textColor=Constants.Color_Default
                viewpage.lblTimeMaghrib.textColor=Constants.Color_Default
                viewpage.lblTimeIsha.textColor=Constants.Color_Default
                viewpage.lblTimeSunrise.textColor=Constants.Color_Default
                
                
                viewpage.lblSubuh.textColor=Constants.Color_Highlighted
                viewpage.lblTimeSubuh.textColor=Constants.Color_Highlighted
            
            }

        
        }
        
    }
    
    
    func addNotification (objPrayerTimeForNotify:PrayerTime , strDate: String)  {
        
        var Subuh : String = objPrayerTimeForNotify.subuh!
        var Zuhr: String = objPrayerTimeForNotify.luhar!
        let Asar_Hanafi: String = objPrayerTimeForNotify.asar_hanafi!
        let Asar_Shafi: String = objPrayerTimeForNotify.asar_shafi!
        var Maghrib: String = objPrayerTimeForNotify.magrib!
        let Isha_Hanafi: String = objPrayerTimeForNotify.isha_hanafi!
        let Isha_Shafi: String = objPrayerTimeForNotify.isha_shafi!
       // var Sunrise: String = objPrayerTimeForNotify.sunrise!
        
        
        
        Subuh=Subuh+" a"
        Zuhr=Zuhr+" p"
        var Asar: String = ""
        var Isha: String = ""
        if(appDelegate.isAhar1())
        {
            Asar = Asar_Hanafi + " p"
            Isha = Isha_Hanafi + " p"
        }
        else
        {
            Asar =  Asar_Shafi + " p"
            Isha = Isha_Shafi + " p"
        }
        Maghrib=Maghrib+" p"
        
//        Sunrise=Sunrise+" a"
        
        
        
        var arrDate:[Date]=[]
        var arrValues:[String]=[]
        
        var dateObj:Date = generateDateFromStringForNotification(strDate: strDate, strTime: Subuh)
        
        arrDate.append(dateObj)
        arrValues.append(Subuh)
        
        dateObj = generateDateFromStringForNotification(strDate: strDate, strTime: Zuhr)
        
        arrDate.append(dateObj)
        arrValues.append(Zuhr)
        
        dateObj = generateDateFromStringForNotification(strDate: strDate, strTime: Asar)
        
        arrDate.append(dateObj)
        arrValues.append(Asar)
        
        dateObj = generateDateFromStringForNotification(strDate: strDate, strTime: Maghrib)
        
        arrDate.append(dateObj)
        arrValues.append(Maghrib)
        
        dateObj = generateDateFromStringForNotification(strDate: strDate, strTime: Isha)
        
        arrDate.append(dateObj)
        arrValues.append(Isha)
        
//        dateObj = generateDateFromStringForNotification(strDate: strDate, strTime: Sunrise)
//        
//        arrDate.append(dateObj)
//        arrValues.append(Sunrise)
        //  print("Dateobj: \(dateObj)")
        
        for  i  in (0..<5) {
            
            let d:Date=arrDate[i]
            
            if  Constants.arrKeysToDisplay[i] == "Sunrise"
            {
//                let strNotificationSoundName : String = appDelegate.userDefaults!.value(forKey: "NotificationSoundName") as! String + ".wav"
//                
//                let elapsed = d.timeIntervalSince(NSDate() as Date)
//                
//                if  ( elapsed > 0)
//                {
//                    let str_requestIdentifier : String = strDate + Constants.arrKeysToDisplay[i]
//                        
//                    triggerNotification(str_requestIdentifier:str_requestIdentifier,
//                                        title: Constants.arrKeysToDisplay[i] + " Tap or Swipe for full",
//                                        subtitle: "", body: arrValues[i],
//                                        soundName: strNotificationSoundName,
//                                        timeIntervalValue:elapsed,
//                                        date: d)
//                    
//                    
//                     print("Notification Added " + Constants.arrKeysToDisplay[i] + arrValues[i])
//                }
            }
            else
            {
                let strValue : String = appDelegate.userDefaults!.value(forKey: Constants.arrKeysToDisplay[i]) as! String
                
                if strValue == "YES"
                {
                   
                    
                    let strNotificationSoundName : String = appDelegate.userDefaults!.value(forKey: "NotificationSoundName") as! String + ".wav"
                    
                    let elapsed = d.timeIntervalSince(NSDate() as Date)
                    if  ( elapsed > 0)
                    {
                        let str_requestIdentifier : String = strDate + Constants.arrKeysToDisplay[i]
                        
                        
                       let str_notification : String = "It is the time for " + Constants.arrKeysToDisplay[i]
                        triggerNotification(str_requestIdentifier:str_requestIdentifier,
                                            title: str_notification,
                                            subtitle: "", body: arrValues[i],
                                            soundName: strNotificationSoundName,
                                            timeIntervalValue:elapsed,
                                            date: d)
                        
                        print("Notification Added " + Constants.arrKeysToDisplay[i] + arrValues[i])
                    }
                }
            }

        }
        
        
    }
    
    func generateDateFromStringForNotification(strDate: String ,strTime: String) -> Date
    {
        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "MM-dd-yyyy"
//        
//        let dateString = dateFormatter.string(from:NSDate() as Date)
        //        print(dateString)
        
        let strDateToConvert:String = strDate + " " + strTime
        
        
        //print("strDateToConvert:" + strDateToConvert)
        
        // print("strDate: \(strDateSubuh)")
        dateFormatter.dateFormat = "MM-dd-yyyy hh:mm a"
        
        let dateObj:Date  = dateFormatter.date(from: strDateToConvert)! as Date
        
        return dateObj
    }
    
    func generateDateFromString(strDate: String) -> Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy"
        
        let dateString = dateFormatter.string(from:NSDate() as Date)
//        print(dateString)
        
        let strDateToConvert:String = dateString + " " + strDate

        
        
        // print("strDate: \(strDateSubuh)")
        dateFormatter.dateFormat = "MM-dd-yyyy hh:mm a"
        
        let dateObj:Date  = dateFormatter.date(from: strDateToConvert)! as Date
        
        return dateObj
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func dateDiff(startDate:Date , endDate:Date) -> String {
       
        
        let calendar = NSCalendar.current
        let unitFlags = Set<Calendar.Component>([ .weekOfMonth,.day, .month, .year, .hour,.minute,.second])
        let dateComponents = calendar.dateComponents(unitFlags, from: startDate,  to: endDate)
        
        let weeks = abs(dateComponents.weekOfMonth!)
        let days = abs(dateComponents.day!)
        let hours = abs(dateComponents.hour!)
        let min = abs(dateComponents.minute!)
        let sec = abs(dateComponents.second!)
        
        var timeAgo = ""

        
        if(weeks > 0){
            if (weeks > 1) {
                timeAgo = "\(timeAgo) \(weeks) Weeks "
            } else {
                timeAgo = "\(timeAgo) \(weeks) Week "
            }
        }
        
        if (days > 0) {
            if (days > 1) {
                timeAgo = "\(timeAgo) \(days) D : "
            } else {
                timeAgo = "\(timeAgo) \(days) D : "
            }
        }
        
        if(hours > 0){
            if (hours >= 1 &&  hours <= 9 ) {
                timeAgo = "\(timeAgo) 0\(hours)H:"
            } else {
                timeAgo = "\(timeAgo) \(hours)H:"
            }
        }
        else
        {
            timeAgo = "\(timeAgo)00H:"
        }
        
        if(min > 0){
            if (min >= 1 &&  min <= 9 ) {
                timeAgo = "\(timeAgo)0\(min)M:"
            } else {
                timeAgo = "\(timeAgo)\(min)M:"
            }
        }
        else
        {
            timeAgo = "\(timeAgo)00M:"
        }
        
        if(sec > 0){
            if (sec >= 1 &&  sec <= 9 ) {
                timeAgo = "\(timeAgo)0\(sec)S"
            } else {
                timeAgo = "\(timeAgo)\(sec)S"
            }
        }
        else
        {
            timeAgo = "\(timeAgo)00S"
        }
        
        
//        print("timeAgo is===> \(timeAgo)")
        return timeAgo;
    }
    
    func dateDiffWatch(startDate:Date , endDate:Date) -> String {
        
        
        let calendar = NSCalendar.current
        let unitFlags = Set<Calendar.Component>([ .weekOfMonth,.day, .month, .year, .hour,.minute,.second])
        let dateComponents = calendar.dateComponents(unitFlags, from: startDate,  to: endDate)
        
//        let weeks = abs(dateComponents.weekOfMonth!)
//        let days = abs(dateComponents.day!)
        let hours = abs(dateComponents.hour!)
        let min = abs(dateComponents.minute!)
        let sec = abs(dateComponents.second!)
        
        var timeAgo = ""
        
//        if(weeks > 0){
//            if (weeks > 1) {
//                timeAgo = "\(timeAgo) \(weeks)"
//            } else {
//                timeAgo = "\(timeAgo) \(weeks)"
//            }
//        }
//        
//        if (days > 0) {
//            if (days > 1) {
//                timeAgo = "\(timeAgo) \(days)"
//            } else {
//                timeAgo = "\(timeAgo) \(days)"
//            }
//        }
        
        if(hours > 0){
            if (hours > 0 && hours < 10 ) {
                timeAgo = "\(timeAgo) 0\(hours):"
            } else {
                timeAgo = "\(timeAgo) \(hours):"
            }
        }
        else
        {
            timeAgo = "\(timeAgo) 00:"
        }
        
        if (min > 0){
            if (min > 0 && min < 10 ) {
                timeAgo = "\(timeAgo) 0\(min):"
            } else {
                timeAgo = "\(timeAgo) \(min):"
            }
        }
        else
        {
            timeAgo = "\(timeAgo) 00:"
        }

        if (sec > 0  ){
            if (sec > 0 && sec < 10) {
                timeAgo = "\(timeAgo) 0\(sec)"
            } else {
                timeAgo = "\(timeAgo) \(sec)"
            }
        }
        else
        {
            timeAgo = "\(timeAgo) 00"
        }

        
        
//        print("timeAgo is===> \(timeAgo)")
        return timeAgo;
    }
    
    // MARK: - Local notification
    func triggerNotificationTest( timevalue : TimeInterval , strTitle :String){
        
       // print("notification will be triggered in five seconds..Hold on tight")
        if #available(iOS 10.0, *) {
            let content = UNMutableNotificationContent()
            content.title = strTitle
            content.subtitle = strTitle + "subtitle"
            content.body = strTitle
            
            let strNotificationSoundName : String = appDelegate.userDefaults!.value(forKey: "NotificationSoundName") as! String + ".wav"
            
            content.sound = UNNotificationSound.init(named:strNotificationSoundName)  //
            //content.sound = UNNotificationSound.default()
            
            
            //To Present image in notification
            //        if let path = Bundle.main.path(forResource: "monkey", ofType: "png") {
            //            let url = URL(fileURLWithPath: path)
            //
            //            do {
            //                let attachment = try UNNotificationAttachment(identifier: "sampleImage", url: url, options: nil)
            //                content.attachments = [attachment]
            //            } catch {
            //                print("attachment not found.")
            //            }
            //        }
            
            // Deliver the notification in five seconds.
            let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: timevalue, repeats: false)
            let requestIdentifier = strTitle //identifier is to cancel the notification request
            
            let request = UNNotificationRequest(identifier:requestIdentifier, content: content, trigger: trigger)
            
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().add(request){(error) in
                
                if (error != nil){
                    
                    print(error?.localizedDescription as Any)
                }
            }
            
            
            print("Test Notification added " + strTitle )

        } else {
            // Fallback on earlier versions
            
            let startDate = Date()
            let calendar = Calendar.current
            let date = calendar.date(byAdding: .second, value:Int(timevalue), to: startDate)
            
            let localNotification = UILocalNotification()
            localNotification.fireDate = date
            localNotification.alertBody = strTitle
            localNotification.alertAction = "View List"
            UIApplication.shared.scheduleLocalNotification(localNotification)
        }
        
    }

    func triggerNotification(str_requestIdentifier :String ,title :String ,subtitle :String ,body:String,soundName:String ,timeIntervalValue:TimeInterval, date:Date){
        
       // print("notification will be triggered ")
        if #available(iOS 10.0, *) {
            let content = UNMutableNotificationContent()
            content.title = title //"Intro to Notifications"
            content.subtitle = subtitle // "Lets code,Talk is cheap"
            content.body = body //"Sample code from WWDC"
            content.sound = UNNotificationSound.init(named:soundName) //"notification.mp3"
            
            //To Present image in notification
            //        if let path = Bundle.main.path(forResource: "monkey", ofType: "png") {
            //            let url = URL(fileURLWithPath: path)
            //
            //            do {
            //                let attachment = try UNNotificationAttachment(identifier: "sampleImage", url: url, options: nil)
            //                content.attachments = [attachment]
            //            } catch {
            //                print("attachment not found.")
            //            }
            //        }
            
            // Deliver the notification in five seconds.
            let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: timeIntervalValue, repeats: false)
            
            let requestIdentifier = str_requestIdentifier //identifier is to cancel the notification request
            
            let request = UNNotificationRequest(identifier:requestIdentifier, content: content, trigger: trigger)
            
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().add(request){(error) in
                
                if (error != nil){
                    
                    print(error?.localizedDescription as Any)
                }
            }

        }
        else
        {
        
        
            let localNotification = UILocalNotification()
            localNotification.fireDate = date
            localNotification.alertBody = body
            localNotification.alertAction = title
            UIApplication.shared.scheduleLocalNotification(localNotification)
        }
        
    }

// MARK: - UICollectionView
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return appDelegate.numDays-appDelegate.intdayString+1
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let reuseIdentifier = "cellCalDay"
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! cellCalDay
        
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
//        cell.myLabel.text = self.items[indexPath.item]
        
        
        let val:Int? = Int(appDelegate.intdayString)
        
        
        let ans = val!+Int(indexPath.row)
        
        cell.lblDateDigit.text = String(ans)
        
         let today=Date()
        let DateValue = Calendar.current.date(byAdding: .day, value: indexPath.row, to: today)
        let strDay=Show_DateDayFirstCharacter(DateValue!)
        
        cell.lblDayChar.text=strDay
        
        cell.imgSelectedDate.layer.cornerRadius = cell.imgSelectedDate.frame.size.width / 2
        cell.imgSelectedDate.clipsToBounds = true
        
        if indexPath.row==selectedIndex {
            cell.imgSelectedDate.isHidden=false
        }else
        {
            cell.imgSelectedDate.isHidden=true
        }
        
        //cell.backgroundColor = UIColor.cyan // make cell more visible in our example project
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        //print("You selected cell #\(indexPath.item)!")
        
//        let today=Date()
//        let DateValue = Calendar.current.date(byAdding: .day, value: indexPath.row, to: today)
        
//        Show_currentDate(DateValue!)
//        Show_IslamicDate(DateValue!)
        
        selectedIndex=indexPath.row
        
        viewPager.scrollToPage(indexPath.item+1)
        
       
        
        collectionView.reloadData()
        
    }
    
    func pressButton(button: UIButton) {
        NSLog("pressed!")
        
        
       
        
      
        
        button.isSelected=(!button.isSelected)
        
        var strValueYESNO:String=""
        
         var strKey:String=""
        
        if button.isSelected {
            strValueYESNO="YES"
        }
        else{
            strValueYESNO="NO"
        }
        
        
        
        let viewObj:ViewPagerCustom=arrViewPagesAll[0] as! ViewPagerCustom
        
        
        if  button == viewObj.btnSubuh
        {
            strKey="Subuh"
        }
        else if  button == viewObj.btnZuhr
        {
            strKey="Zuhr"
        }
        else if  button == viewObj.btnAsar
        {
            strKey="Asar"
        }
        else if  button == viewObj.btnMaghrib
        {
             strKey="Maghrib"
        }
        else if  button == viewObj.btnIsha
        {
             strKey="Isha"
        }

        
        
        appDelegate.userDefaults!.set(strValueYESNO, forKey: strKey)
        
        //print(strKey," : ", strValueYESNO)
        
    }

}


extension HomeViewController:ViewPagerDataSource{
    func numberOfItems(_ viewPager:ViewPager) -> Int {
        return appDelegate.numDays-appDelegate.intdayString+1
    }
    
    func viewAtIndex(_ viewPager:ViewPager, index:Int, view:UIView?) -> UIView {
        let newView:ViewPagerCustom = arrViewPagesAll.object(at: index) as! ViewPagerCustom;
       
        if (index != 0 ){
            
            newView.btnAsar.isHidden=true
            newView.btnIsha.isHidden=true
            newView.btnZuhr.isHidden=true
            newView.btnSubuh.isHidden=true
            newView.btnMaghrib.isHidden=true
            
            
        }
        else
        {
            
            newView.btnAsar.isHidden=false
            newView.btnIsha.isHidden=false
            newView.btnZuhr.isHidden=false
            newView.btnSubuh.isHidden=false
            newView.btnMaghrib.isHidden=false
            
            //            "Subuh"
            //            ,"Zuhr"
            //            ,"Asar"
            //            ,"Maghrib"
            //            ,"Isha"
            let resultSubuh:String = appDelegate.userDefaults!.value(forKey: "Subuh") as! String
            let resultZuhr:String = appDelegate.userDefaults!.value(forKey: "Zuhr") as! String
            let resultAsar:String = appDelegate.userDefaults!.value(forKey: "Asar") as! String
            let resultMaghrib:String = appDelegate.userDefaults!.value(forKey: "Maghrib") as! String
            let resultIsha:String = appDelegate.userDefaults!.value(forKey: "Isha") as! String
            
            
            newView.btnSubuh.isSelected = (resultSubuh == "YES") ? true:false
            newView.btnZuhr.isSelected = (resultZuhr == "YES") ? true:false
            newView.btnAsar.isSelected = (resultAsar == "YES") ? true:false
            newView.btnMaghrib.isSelected = (resultMaghrib == "YES") ? true:false
            newView.btnIsha.isSelected = (resultIsha == "YES") ? true:false
        }
        
        
        return newView as UIView
    }
    
    func didSelectedItem(_ index: Int) {
       // print("select index \(index)")
        
        
    //Show_selectedView(indexValue: index)
    }
    func finalCurrentItem(_ index: Int) {
      //  print("finalCurrentItem select index \(index)")
        
        
        Show_selectedView(index)
    }
    
    
}

extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}


extension UIColor {
    static func randomColor() -> UIColor {
        // If you wanted a random alpha, just create another
        // random number for that too.
        return UIColor(red:   .random(),
                       green: .random(),
                       blue:  .random(),
                       alpha: 1.0)
    }
}

extension HomeViewController:UNUserNotificationCenterDelegate{
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
       // print("Tapped in notification")
        
//        let strNotificationSoundName : String = "adhan"
//        
//        
//      //  print("fileName to Play = \(strNotificationSoundName)")
//        
//        
//        
//        let url = Bundle.main.url(forResource: strNotificationSoundName, withExtension: "wav")!
//        
//        do {
//            player = try AVAudioPlayer(contentsOf: url)
//            guard let player = player else { return }
//            //            player.delegate=self
//            player.prepareToPlay()
//            player.play()
//            
//        } catch let error {
//            print(error.localizedDescription)
//        }
//        
        
        
       

    }
    
    //This is key callback to present notification while the app is in foreground
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
       // print("Notification being triggered")
        //You can either present alert ,sound or increase badge while the app is in foreground too with ios 10
        //to distinguish between notifications
        //if notification.request.identifier == requestIdentifier{
            
            completionHandler( [.alert,.sound,.badge])
            
          
            fetch()
            
            
            
        //}
    }
}
