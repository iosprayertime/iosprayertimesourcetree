//
//  InterfaceController.swift
//  PrayerTimesWatch Extension
//
//  Created by DevoGuru on 15/11/16.
//  Copyright © 2016 Vipul. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity


class InterfaceController: WKInterfaceController,WCSessionDelegate {
    /** Called when the session has completed activation. If session state is WCSessionActivationStateNotActivated there will be an error with more details. */
    

    
    
    @IBOutlet var lblTitleAfter: WKInterfaceLabel!
    
    @IBOutlet var lblRemainTime: WKInterfaceLabel!

    @IBOutlet var lblPreviousTitle: WKInterfaceLabel!
    @IBOutlet var lblPreviousTime: WKInterfaceLabel!
    
    @IBOutlet var lblNextTitle: WKInterfaceLabel!
    @IBOutlet var lblNextTime: WKInterfaceLabel!
    
    
   
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        
        let watchSession = WCSession.default()
        watchSession.delegate = self
        watchSession.activate()
        
        lblTitleAfter.setText("")
        lblRemainTime.setText("")
        
        lblPreviousTitle.setText("")
        lblPreviousTime.setText("")
        
        lblNextTitle.setText("")
        lblNextTime.setText("")
        
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        
        
        
        
        
    }
    
    @available(watchOS 2.2, *)
    public func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
       //  print("session")
        
        
        
    }
    
    func session(_ session: WCSession, didReceiveApplicationContext applicationContext: [String : Any]) {
       // print("didReceiveApplicationContext")
        
        
//                                                    "Watch_TimeActive_Title":strWatch_TimeActive_Title,
//                                                   "Watch_TimeActive_Time":strDateDiff,
//                                                   "Watch_Previous_Title":strWatch_Previous_Title,
//                                                   "Watch_Previous_Time":strWatch_Previous_Time,
//                                                   "Watch_Next_Title":strWatch_Next_Title,
//                                                   "Watch_Next_Time":strWatch_Next_Time])
        
        
        
        if let strWatch_TimeActive_Title:String =   applicationContext["Watch_TimeActive_Title"] as? String {
            lblTitleAfter.setText(strWatch_TimeActive_Title + " after")
        }
        else
        {
            lblTitleAfter.setText("")
        
        }
        
        if let strWatch_Time_DateDiff:String = applicationContext["Watch_TimeActive_Time"] as? String{
            
        
            lblRemainTime.setText(strWatch_Time_DateDiff)
        }
        else
        {
            lblRemainTime.setText("")
            
        }
 
        if   let strWatch_Previous_Title:String =  applicationContext["Watch_Previous_Title"] as? String{
             lblPreviousTitle.setText(strWatch_Previous_Title)
        }
        else
        {
             lblPreviousTitle.setText("")
        }
        if     let strWatch_Previous_Time:String = applicationContext["Watch_Previous_Time"] as? String {
            
            
            var newString = strWatch_Previous_Time.replacingOccurrences(of: "p", with: "PM")
            newString = newString.replacingOccurrences(of: "a", with: "AM")
             lblPreviousTime.setText(newString)
        }
        else
        {
            lblPreviousTime.setText("")
        }
        
        
            
        if    let strWatch_Next_Title:String =  applicationContext["Watch_Next_Title"] as? String{
             lblNextTitle.setText(strWatch_Next_Title)
        }
        else{
             lblNextTitle.setText("")
        }
        
        
        if let strWatch_Next_Time:String = applicationContext["Watch_Next_Time"] as? String{
            
            var newString = strWatch_Next_Time.replacingOccurrences(of: "p", with: "PM")
            newString = newString.replacingOccurrences(of: "a", with: "AM")
            
            lblNextTime.setText(newString)
        }
        else{
            lblNextTime.setText("")
        }
        
        
        
        
        
        

        
    }
    
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
