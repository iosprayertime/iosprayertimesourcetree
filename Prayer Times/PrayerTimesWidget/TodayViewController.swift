//
//  TodayViewController.swift
//  PrayerTimesWidget
//
//  Created by DevoGuru on 15/11/16.
//  Copyright © 2016 Vipul. All rights reserved.
//

import UIKit
import NotificationCenter

class TodayViewController: UIViewController, NCWidgetProviding {
   
   
    
    var objPrayerTimeCurrent:PrayerTime? = nil
    var objPrayerTimeCurrentNextDay:PrayerTime? = nil
    
     var intdayString: Int=0
    var monthString:String=""
    
    var timer : Timer?
   
    @IBOutlet var imageBg: UIImageView!
    
    @IBOutlet weak var lblCurrentTime: UILabel!
    @IBOutlet weak var lblCurrentDate: UILabel!
    
    @IBOutlet weak var lblCurrentDateIslamic: UILabel!
    
    @IBOutlet weak var btnSubuh: UIButton!
    @IBOutlet weak var btnZuhr: UIButton!
    @IBOutlet weak var btnAsar: UIButton!
    @IBOutlet weak var btnMaghrib: UIButton!
    @IBOutlet weak var btnIsha: UIButton!
    
    @IBOutlet var viewSubuh: UIView!
    @IBOutlet var viewZuhr: UIView!
    @IBOutlet var viewAsar: UIView!
    @IBOutlet var viewMaghrib: UIView!
    @IBOutlet var viewIsha: UIView!
    @IBOutlet var viewSunrise: UIView!
    
    
    
    @IBOutlet weak var lblTitleSubuh: UILabel!
    @IBOutlet weak var lblTitleZuhr: UILabel!
    @IBOutlet weak var lblTitleAsar: UILabel!
    @IBOutlet weak var lblTitleMaghrib: UILabel!
    @IBOutlet weak var lblTitleIsha: UILabel!
    @IBOutlet weak var lblTitleSunrise: UILabel!
    
    
    @IBOutlet weak var lblTimeSubuh: UILabel!
    @IBOutlet weak var lblTimeZuhr: UILabel!
    @IBOutlet weak var lblTimeAsar: UILabel!
    @IBOutlet weak var lblTimeMaghrib: UILabel!
    @IBOutlet weak var lblTimeIsha: UILabel!
    @IBOutlet weak var lblTimeSunrise: UILabel!
    
    
    
    @IBOutlet weak var lblRemainingTime: UILabel!
    
    
    
    let userDefaults = UserDefaults(suiteName: "group.com.quthubuzaman.kayalprayertimes")
    
    func isAhar1() -> Bool {
        
        
        let strASAR : String = userDefaults!.value(forKey: "ASAR_CALCULATION_METHOD") as! String
        
        
        if  strASAR == "ASAR 1"
        {
            return true
        }
        else
        {
            return false
        }
        
    }

    @IBAction func launchHostingApp(_ sender: Any) {
        
        extensionContext?.open(NSURL(string: "openApp://")! as URL, completionHandler: nil)

        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view from its nib.
        
        
    
        
       
        self.preferredContentSize=CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height)
       
        
//         self.extensionContext?.widgetLargestAvailableDisplayMode = NCWidgetDisplayMode.expanded
        
        if timer != nil {
            timer!.invalidate()
            timer = nil
        }
        
        imageBg.backgroundColor = Constants.Color_Highlighted
        
      
        
        
        
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "MMMM"
        
        let currentDate=Date()
        
        monthString = dayTimePeriodFormatter.string(from: currentDate)
        
        monthString=monthString.lowercased()
        
        print("Date :",monthString);
        
        
        
        dayTimePeriodFormatter.dateFormat = "d"
        let dayString : String = dayTimePeriodFormatter.string(from: currentDate)
        print("Current Day :",dayString);
        
        
        dayTimePeriodFormatter.dateFormat = "YYYY"
        let yearString : String = dayTimePeriodFormatter.string(from: currentDate)
        print("Current Year :",yearString);
        
        
        intdayString = Int(dayString)!
        
       let totalRecords:Int = CoreDataStack.recordsInTable(PrayerTime.self)
        
        if (totalRecords > 0)
        {
            print("Records Found ", totalRecords )
        }
        else
        {
            //myCoreDataObj.deleteAllRecords(PrayerTime.self)
            
            
            print("Records Not Found ")
            // kayaldata  . txt    chennaidata   . txt
            if let path = Bundle.main.path(forResource: "chennaidata", ofType: "txt") {
                do {
                    let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                    
                    
                    let parsedData = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:Any]
                    
                    //print(parsedData["january"] as Any);
                    
                    
                    
                    for monthname in Constants.arrMonthName {
                        
                        print(monthname! as String);
                        
                        let dic:NSDictionary = parsedData[monthname!] as! NSDictionary
                        
                        // print(dic)
                        
                        for i in (1..<31+1)
                        {
                            let strDate=String(i)
                            if ((dic[strDate]) != nil)
                            {
                                let dicValues:NSDictionary = dic[strDate] as! NSDictionary
                                
                                //print("Date ", strDate, "Value ",dicValues)
                                
                                
                                let valAsr_Hanafi:String = dicValues[Constants.KEYS_Asr_Hanafi] as! String ;
                                let valAsr_Shafi:String = dicValues[Constants.KEYS_Asr_Shafi] as! String ;
                                let valIsha_Hanafi:String = dicValues[Constants.KEYS_Isha_Hanafi] as! String ;
                                let valIsha_Shafi:String = dicValues[Constants.KEYS_Isha_Shafi] as! String ;
                                let valZuhr :String = dicValues[Constants.KEYS_Zuhr] as! String ;
                                let valMaghrib:String = dicValues[Constants.KEYS_Maghrib] as! String ;
                                let valsubuh:String = dicValues[Constants.KEYS_Subuh] as! String ;
                                let valsunrise :String = dicValues[Constants.KEYS_Sunrise] as! String ;
                                
                                
                                
                                let p = CoreDataStack.addRecord(PrayerTime.self)
                                
                                p.address=""
                                p.area=""
                                
                                p.dayDigit=Int16(i)
                                p.day=strDate
                                p.month=monthname
                                p.year=yearString
                                
                                p.asar_hanafi=valAsr_Hanafi
                                p.asar_shafi=valAsr_Shafi
                                p.isha_hanafi=valIsha_Hanafi
                                p.isha_shafi=valIsha_Shafi
                                
                                p.luhar=valZuhr
                                p.magrib=valMaghrib
                                p.subuh=valsubuh
                                p.sunrise=valsunrise
                                
                                
                                CoreDataStack.saveDatabase()
                                
                                
                                
                            }
                            
                        }
                        
                        
                        
                    }
                    
                    print("Final");
                    
                    
                } catch let error {
                    print(error.localizedDescription)
                }
            } else {
                print("Invalid filename/path.")
            }
            
            
            print("Records Inserted from File .. Done.. ")
        }
        
        
        let resultPredicate1: NSPredicate = NSPredicate(format: "month = %@ and dayDigit = %d", monthString, intdayString)
        
        let resultSort: NSSortDescriptor=NSSortDescriptor(key: "dayDigit", ascending: true)
        
       let arrResult:[PrayerTime?]  = CoreDataStack.query(PrayerTime.self, search: resultPredicate1, sort: resultSort, multiSort: nil )
        
        
        objPrayerTimeCurrent=arrResult[0]
        
        var arrResult_nextday:[PrayerTime?]  = nextarrvalue(strmonth: monthString , intday:intdayString + 1 )
        
        
        if ( arrResult_nextday.count > 0 )
        {
            
        }
        else{
            
            let indx:Int = Constants.arrMonthName.index(where: { $0 ==  monthString })!
            
            arrResult_nextday = nextarrvalue(strmonth: Constants.arrMonthName[indx+1]! , intday: 1 )
            
        }
        
        
        objPrayerTimeCurrentNextDay=arrResult_nextday[0]
        
        
          let today=Date()
        Show_currentDate(today)
        Show_IslamicDate(today)
        
        
        Show_RemainingTime()
//
        if timer == nil {
            timer = Timer.scheduledTimer(timeInterval:1, target: self, selector: #selector(Show_RemainingTime), userInfo: nil, repeats: true)
            
        }
        
        
        let resultSubuh:String = userDefaults!.value(forKey: "Subuh") as! String
        let resultZuhr:String = userDefaults!.value(forKey: "Zuhr") as! String
        let resultAsar:String = userDefaults!.value(forKey: "Asar") as! String
        let resultMaghrib:String = userDefaults!.value(forKey: "Maghrib") as! String
        let resultIsha:String = userDefaults!.value(forKey: "Isha") as! String
        
        
//        print("resultSubuh " + resultSubuh)
//        print("resultZuhr " + resultZuhr)
//        print("resultAsar " + resultAsar)
//        print("resultMaghrib " + resultMaghrib)
//        print("resultIsha " + resultIsha)
        
        btnSubuh.isSelected = (resultSubuh == "YES") ? true:false
        btnZuhr.isSelected = (resultZuhr == "YES") ? true:false
        btnAsar.isSelected = (resultAsar == "YES") ? true:false
        btnMaghrib.isSelected = (resultMaghrib == "YES") ? true:false
        btnIsha.isSelected = (resultIsha == "YES") ? true:false
        
        
        let Subuh : String = userDefaults!.value(forKey: "TimeSubuh") as! String
        let Zuhr: String = userDefaults!.value(forKey: "TimeZuhr") as! String
        let Asar: String = userDefaults!.value(forKey: "TimeAsar") as! String
        let Maghrib: String = userDefaults!.value(forKey: "TimeMaghrib") as! String
        let Isha: String = userDefaults!.value(forKey: "TimeIsha") as! String
        let Sunrise: String = userDefaults!.value(forKey: "TimeSunrise") as! String
        
        lblTimeSubuh.text = Subuh
        lblTimeZuhr.text = Zuhr
        lblTimeAsar.text = Asar
        lblTimeMaghrib.text = Maghrib
        lblTimeIsha.text = Isha
        lblTimeSunrise.text = Sunrise
        
//        let RemainingTime: String = userDefaults!.value(forKey: "RemaingTime") as! String
//        
//        lblRemainingTime.text = RemainingTime
        

        
    }
  
    
  
    
    func nextarrvalue(strmonth:String , intday : Int) -> [PrayerTime?]  {
       
        
        let resultPredicate1_nextday: NSPredicate = NSPredicate(format: "month = %@ and dayDigit = %d", strmonth, intday)
        
        let arrResult_nextday:[PrayerTime?]  = CoreDataStack.query(PrayerTime.self, search: resultPredicate1_nextday, sort: nil, multiSort: nil )
        
        return arrResult_nextday
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        // Perform any setup necessary in order to update the view.
        
        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData
        
        completionHandler(NCUpdateResult.newData)
    }
    func widgetActiveDisplayModeDidChange(_ activeDisplayMode: NCWidgetDisplayMode, withMaximumSize maxSize: CGSize){
        if (activeDisplayMode == NCWidgetDisplayMode.compact) {
            self.preferredContentSize = maxSize;
        }
        else {
            self.preferredContentSize = CGSize(width: 0, height: 113);
        }
    }
    
    func Show_currentDate(_ date: Date) {
        
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "EEE,dd MMM YYYY"
        
        let currentDate=date
        
        let dateString = dayTimePeriodFormatter.string(from: currentDate)
        
        lblCurrentDate.text = dateString
        
        dayTimePeriodFormatter.dateFormat = "hh : mm a"
        
        let datetimeString = dayTimePeriodFormatter.string(from: currentDate)
        
        lblCurrentTime.text = datetimeString
        
    }
    func Show_IslamicDate(_ date: Date){
        
        
        let strHIJRI_DATE : String = userDefaults!.value(forKey: "ADJUST_HIJRI_DATE") as! String
        
        let dayToAdd:Double = Double(strHIJRI_DATE)!
        
        //        dateByAddingTimeInterval:60*60*24*daysToAdd
        let newDate: Date = date.addingTimeInterval(60 * 60 * 24 * dayToAdd)
        
        let myLocale = Locale .current
        var calendar = Calendar(identifier: .islamic)
        calendar.locale = myLocale
        
        
        //: ### Fetching `DateComponents` off a `Date`
        //: Notice how *a locale is needed for the month symbols to be reported correctly*
        let dateComponents = calendar.dateComponents([.day, .month, .year], from: newDate)
//        let monthName = calendar.monthSymbols[dateComponents.month! - 1]
        let monthName:String = Constants.arrMonthNameIslamic[dateComponents.month! - 1]!
        
        let islamicName="\(dateComponents.day!)-\(monthName)-\(dateComponents.year!)"
        lblCurrentDateIslamic.text = islamicName
        
    }
    
    
    func generateDateFromString(strDate: String) -> Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy"
        
        let dateString = dateFormatter.string(from:NSDate() as Date)
        //        print(dateString)
        
        let strDateToConvert:String = dateString + " " + strDate
        
        
        
        // print("strDate: \(strDateSubuh)")
        dateFormatter.dateFormat = "MM-dd-yyyy hh:mm a"
        
        let dateObj:Date  = dateFormatter.date(from: strDateToConvert)! as Date
        
        return dateObj
    }
    func dateDiff(startDate:Date , endDate:Date) -> String {
        
        
        let calendar = NSCalendar.current
        let unitFlags = Set<Calendar.Component>([ .weekOfMonth,.day, .month, .year, .hour,.minute,.second])
        let dateComponents = calendar.dateComponents(unitFlags, from: startDate,  to: endDate)
        
        let weeks = abs(dateComponents.weekOfMonth!)
        let days = abs(dateComponents.day!)
        let hours = abs(dateComponents.hour!)
        let min = abs(dateComponents.minute!)
        let sec = abs(dateComponents.second!)
        
        var timeAgo = ""
        
        
        if(weeks > 0){
            if (weeks > 1) {
                timeAgo = "\(timeAgo) \(weeks) Weeks "
            } else {
                timeAgo = "\(timeAgo) \(weeks) Week "
            }
        }
        
        if (days > 0) {
            if (days > 1) {
                timeAgo = "\(timeAgo) \(days) D : "
            } else {
                timeAgo = "\(timeAgo) \(days) D : "
            }
        }
        
        if(hours > 0){
            if (hours >= 1 &&  hours <= 9 ) {
                timeAgo = "\(timeAgo) 0\(hours)H:"
            } else {
                timeAgo = "\(timeAgo) \(hours)H:"
            }
        }
        else
        {
            timeAgo = "\(timeAgo)00H:"
        }
        
        if(min > 0){
            if (min >= 1 &&  min <= 9 ) {
                timeAgo = "\(timeAgo)0\(min)M:"
            } else {
                timeAgo = "\(timeAgo)\(min)M:"
            }
        }
        else
        {
            timeAgo = "\(timeAgo)00M:"
        }
        
        if(sec > 0){
            if (sec >= 1 &&  sec <= 9 ) {
                timeAgo = "\(timeAgo)0\(sec)S"
            } else {
                timeAgo = "\(timeAgo)\(sec)S"
            }
        }
        else
        {
            timeAgo = "\(timeAgo)00S"
        }
        
        
        //        print("timeAgo is===> \(timeAgo)")
        return timeAgo;
    }
    
    func Show_RemainingTime()
    {
        
        
        let today=Date()
        Show_currentDate(today)
        Show_IslamicDate(today)
        
        var Subuh : String = objPrayerTimeCurrent!.subuh!
        var Zuhr: String = objPrayerTimeCurrent!.luhar!
        let Asar_Hanafi: String = objPrayerTimeCurrent!.asar_hanafi!
        let Asar_Shafi: String = objPrayerTimeCurrent!.asar_shafi!
        var Maghrib: String = objPrayerTimeCurrent!.magrib!
        let Isha_Hanafi: String = objPrayerTimeCurrent!.isha_hanafi!
        let Isha_Shafi: String = objPrayerTimeCurrent!.isha_shafi!
        var Sunrise: String = objPrayerTimeCurrent!.sunrise!
        
        
        
        Subuh=Subuh+" a"
        Zuhr=Zuhr+" p"
        var Asar: String = ""
        var Isha: String = ""
        if(isAhar1())
        {
            Asar = Asar_Hanafi + " p"
            Isha = Isha_Hanafi + " p"
        }
        else
        {
            Asar =  Asar_Shafi + " p"
            Isha = Isha_Shafi + " p"
        }
        Maghrib=Maghrib+" p"
        
        Sunrise=Sunrise+" a"
        
        
        
        var arrDate:[Date]=[]
        var arrValues:[String]=[]
        
        var dateObj:Date = generateDateFromString(strDate: Subuh)
        
        arrDate.append(dateObj)
        arrValues.append(Subuh)
        
        dateObj = generateDateFromString(strDate:  Zuhr)
        
        arrDate.append(dateObj)
        arrValues.append(Zuhr)
        
        dateObj = generateDateFromString(strDate: Asar)
        
        arrDate.append(dateObj)
        arrValues.append(Asar)
        
        dateObj = generateDateFromString(strDate: Maghrib)
        
        arrDate.append(dateObj)
        arrValues.append(Maghrib)
        
        dateObj = generateDateFromString(strDate:  Isha)
        
        arrDate.append(dateObj)
        arrValues.append(Isha)
        
        dateObj = generateDateFromString(strDate:  Sunrise)
        
        arrDate.append(dateObj)
        arrValues.append(Sunrise)
        //  print("Dateobj: \(dateObj)")
        
        var flag:Bool = false
        
        
        
        for  i  in (0..<5) {
            
            let d:Date=arrDate[i]
            
            let curr:Date = NSDate() as Date
            
            if d > curr
            {
                
                flag = true
                
                //                print("Date greater")
                
                let strDateDiff:String = dateDiff(startDate:d , endDate:NSDate() as Date )
                //                print("strDateDiff:: \(strDateDiff)")
                
                self.lblRemainingTime.text = strDateDiff + " to " + Constants.arrKeysToDisplay[i]
                
            
                self.viewSubuh.backgroundColor=Constants.Color_White
                self.lblTimeSubuh.textColor=Constants.Color_Default
                self.lblTitleSubuh.textColor=Constants.Color_Default
                
                self.viewZuhr.backgroundColor=Constants.Color_White
                self.lblTimeZuhr.textColor=Constants.Color_Default
                self.lblTitleZuhr.textColor=Constants.Color_Default
                
                self.viewAsar.backgroundColor=Constants.Color_White
                self.lblTimeAsar.textColor=Constants.Color_Default
                self.lblTitleAsar.textColor=Constants.Color_Default
                

                self.viewMaghrib.backgroundColor=Constants.Color_White
                self.lblTimeMaghrib.textColor=Constants.Color_Default
                self.lblTitleMaghrib.textColor=Constants.Color_Default
                

                self.viewIsha.backgroundColor=Constants.Color_White
                self.lblTimeIsha.textColor=Constants.Color_Default
                self.lblTitleIsha.textColor=Constants.Color_Default
                
                self.viewSunrise.backgroundColor=Constants.Color_White
                self.lblTimeSunrise.textColor=Constants.Color_Default
                self.lblTitleSunrise.textColor=Constants.Color_Default
                

                
                
                switch i {
                case 0:
                    self.viewSubuh.backgroundColor=Constants.Color_Highlighted
                    self.lblTimeSubuh.textColor=Constants.Color_White
                    self.lblTitleSubuh.textColor=Constants.Color_White
                    break
                case 1:
                    self.viewZuhr.backgroundColor=Constants.Color_Highlighted
                    self.lblTimeZuhr.textColor=Constants.Color_White
                    self.lblTitleZuhr.textColor=Constants.Color_White
                    break
                case 2:
                    self.viewAsar.backgroundColor=Constants.Color_Highlighted
                    self.lblTimeAsar.textColor=Constants.Color_White
                    self.lblTitleAsar.textColor=Constants.Color_White

                    break
                case 3:
                    self.viewMaghrib.backgroundColor=Constants.Color_Highlighted
                    self.lblTimeMaghrib.textColor=Constants.Color_White
                    self.lblTitleMaghrib.textColor=Constants.Color_White
                    break
                    
                case 4:
                    self.viewIsha.backgroundColor=Constants.Color_Highlighted
                    self.lblTimeIsha.textColor=Constants.Color_White
                    self.lblTitleIsha.textColor=Constants.Color_White

                    break
                case 5:
                    self.viewSunrise.backgroundColor=Constants.Color_Highlighted
                    self.lblTimeSunrise.textColor=Constants.Color_White
                    self.lblTitleSunrise.textColor=Constants.Color_White
                    break
                    
                default:
                    break
                }
                
                
                
                break
                
                
                
            }
            else
            {
                //                print("Date less ")
            }
            
        }
        
        if ( flag == false)
        {
            var Subuh : String = objPrayerTimeCurrentNextDay!.subuh!
            Subuh=Subuh+" a"
            
            
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM-dd-yyyy"
            
            
            let nextday : Date = Calendar.current.date(byAdding: DateComponents(day: 1), to: NSDate() as Date)!
            
            let dateString = dateFormatter.string(from:nextday)
            //        print(dateString)
            
            let strDateToConvert:String = dateString + " " + Subuh
            
            
            
            // print("strDate: \(strDateSubuh)")
            dateFormatter.dateFormat = "MM-dd-yyyy hh:mm a"
            
            let dateObj:Date  = dateFormatter.date(from: strDateToConvert)! as Date
            
            
            let strDateDiff:String = dateDiff(startDate:dateObj , endDate:NSDate() as Date )
            //                print("strDateDiff:: \(strDateDiff)")
            
            
            
            
            
            self.lblRemainingTime.text = strDateDiff + " to " + Constants.arrKeysToDisplay[0]
            
            
            
            
            
            
        }
        
    }
    
    
}
