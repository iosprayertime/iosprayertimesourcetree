//
//  Constants.swift
//  Prayer Times
//
//  Created by DevoGuru on 15/11/16.
//  Copyright © 2016 Vipul. All rights reserved.
//

import UIKit


class Constants{
    
    // MARK: List of Constants
    
    static let APP_ALERT_TITLE = "Prayer Times"
    static let kAdUnit = "ca-app-pub-4152079293119594/2974636268"
    
    static let Color_Default:UIColor = UIColor.black
    static let Color_White:UIColor = UIColor.white
    
    static let Color_Highlighted:UIColor = UIColor.init(colorLiteralRed: 56.0/255.0, green: 186.0/255.0, blue: 87.0/255.0, alpha: 1.0)
    
    //          ==== KEYS ====
//   "subuh":"05:44","sunrise":"07:07","luhr":"01:10","Asr":"04:34","Hanafi":"05:29","maghrib":"07:10","Isha":"08:25","IshaH":"08:25"
    
    /*
     === To Display ===
     Subuh
     Zuhr
     Asar
     Maghrib
     Isha
     Sunrise
     */
    static let KEYS_Subuh = "subuh"
    static let KEYS_Zuhr = "luhr"
    static let KEYS_Asr_Shafi = "Asr"
    static let KEYS_Asr_Hanafi = "Hanafi"
    static let KEYS_Maghrib = "maghrib"
    static let KEYS_Isha_Shafi = "Isha"
    static let KEYS_Isha_Hanafi = "IshaH"
    static let KEYS_Sunrise = "sunrise"
    
    static let arrKeysToDisplay:[String]=[
        "Subuh"
        ,"Zuhr"
        ,"Asar"
        ,"Maghrib"
        ,"Isha"
        ,"Sunrise"
    ]
    
    
    static let arrNotificationSoundName:[String?] = [
        "notification"
        ,"thakbeer"
        
    ]
    
    static let arrNotificationSoundFile: [String?]  = [
       "notification.wav"
        ,"thakbeer.wav"
        
    ]
    
    static let arrMonthName:[String?] = [
        "january"
        ,"february"
        ,"march"
        ,"april"
        ,"may"
        ,"june"
        ,"july"
        ,"august"
        ,"september"
        ,"october"
        ,"november"
        ,"december"
        
    ]
    static let arrMonthNameIslamic:[String?] = [
        "Muharram",
        "Safar",
        "Rabi'ul Awwal",
        "Rabi'ul Akhir",
        "Jamathul Awwal",
        "Jamathul Akhir",
        "Rajab",
        "Sha'ban",
        "Ramadan", 
        "Shawwal", 
        "Dhul Qa'ada", 
        "Dhul Haj"
    ]
    
    
}
