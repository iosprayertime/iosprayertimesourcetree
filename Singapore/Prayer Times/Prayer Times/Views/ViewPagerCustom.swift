//
//  ViewPager.swift
//  Prayer Times
//
//  Created by DevoGuru on 11/11/16.
//  Copyright © 2016 Vipul. All rights reserved.
//

import UIKit

class ViewPagerCustom: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var lblDate_islamic: UILabel!
    
    
    
    @IBOutlet weak var lblSubuh: UILabel!
    @IBOutlet weak var lblZuhr: UILabel!
    @IBOutlet weak var lblAsar: UILabel!
    @IBOutlet weak var lblMaghrib: UILabel!
    @IBOutlet weak var lblIsha: UILabel!
    @IBOutlet weak var lblSunrise: UILabel!
    
    
    @IBOutlet weak var lblTimeSubuh: UILabel!
    @IBOutlet weak var lblTimeZuhr: UILabel!
    @IBOutlet weak var lblTimeAsar: UILabel!
    @IBOutlet weak var lblTimeMaghrib: UILabel!
    @IBOutlet weak var lblTimeIsha: UILabel!
    @IBOutlet weak var lblTimeSunrise: UILabel!
    
    
    @IBOutlet weak var btnSubuh: UIButton!
    @IBOutlet weak var btnZuhr: UIButton!
    @IBOutlet weak var btnAsar: UIButton!
    @IBOutlet weak var btnMaghrib: UIButton!
    @IBOutlet weak var btnIsha: UIButton!
    
    
}
